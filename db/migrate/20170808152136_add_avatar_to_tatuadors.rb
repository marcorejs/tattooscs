class AddAvatarToTatuadors < ActiveRecord::Migration[5.0]
  def change
    add_column :tatuadors, :avatar, :string
  end
end
