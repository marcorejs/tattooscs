class AddDescripcionToTatuadors < ActiveRecord::Migration[5.0]
  def change
    add_column :tatuadors, :descripcion, :string
  end
end
