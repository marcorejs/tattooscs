class CreateTatuadors < ActiveRecord::Migration[5.0]
  def change
    create_table :tatuadors do |t|
      t.string :nombre

      t.timestamps
    end
  end
end
