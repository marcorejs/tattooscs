class AddTatuadorsToTattoo < ActiveRecord::Migration[5.0]
  def change
    add_reference :tattoos, :tatuador, foreign_key: true
  end
end
