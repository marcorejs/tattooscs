class AddAvatarToTattoos < ActiveRecord::Migration[5.0]
  def change
    add_column :tattoos, :avatar, :string
  end
end
