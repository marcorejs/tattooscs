json.extract! tatuador, :id, :nombre, :created_at, :updated_at
json.url tatuador_url(tatuador, format: :json)
