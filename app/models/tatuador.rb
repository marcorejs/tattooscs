class Tatuador < ApplicationRecord
    mount_uploader :avatar, AvatarUploader
    has_many:tattoos
end
