Rails.application.routes.draw do

  devise_for :users, controllers: {
    sessions: 'users/sessions',
    registrations: 'users/registrations'
  }

  resources :tatuadors
  resources :tattoos

  root to:'tattoos#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
